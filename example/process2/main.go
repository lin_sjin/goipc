package main

import (
	"gitee.com/mqyqingkong/goipc/client"
	"gitee.com/mqyqingkong/goipc/log"
	"gitee.com/mqyqingkong/goipc/util"
)

var logger = log.GetLogger("ipc-client2")

func main() {
	network := util.GetEnv("ipc_network", "unix")
	address := util.GetEnv("ipc_address", "../../tiny-goipc.sock")

	logger.Infof("create %s IPC client", network)

	ipcClient := client.New(network, address)
	var err error
	err = ipcClient.Auth([]byte("process2:didibaba2"))
	if err != nil {
		logger.Errorf("auth failed: %v", err)
		return
	}

	ipcClient.RegisterReceivers("topic1", &process2BroadcastReceiver1{})
	ipcClient.RegisterReceivers("topic2", &process2BroadcastReceiver2{})

	err = ipcClient.StartReceive()
	if err != nil {
		logger.Errorf("RegisterReceivers failed: %v", err)
		return
	}
	logger.Info("[process-2]started")
	select {}

}

type process2BroadcastReceiver1 struct {
}

func (r *process2BroadcastReceiver1) OnReceive(action *client.InterestedAction) (respPayload []byte) {
	logger.Info("[process2BroadcastReceiver-1]received action:", action)
	return nil
}

type process2BroadcastReceiver2 struct {
}

func (r *process2BroadcastReceiver2) OnReceive(action *client.InterestedAction) (respPayload []byte) {
	logger.Info("[process2BroadcastReceiver-2]received action:", action)
	return []byte("respones to " + action.ActionName)
}
