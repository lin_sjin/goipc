package main

import (
	"fmt"
	"time"

	"gitee.com/mqyqingkong/goipc/client"
	"gitee.com/mqyqingkong/goipc/log"
	"gitee.com/mqyqingkong/goipc/util"
)

var logger = log.GetLogger("ipc-client3")

func main() {
	network := util.GetEnv("ipc_network", "unix")
	address := util.GetEnv("ipc_address", "../../tiny-goipc.sock")

	logger.Infof("create %s IPC client", network)

	ipcClient := client.New(network, address)
	err := ipcClient.Auth([]byte("process3:didibaba3"))
	if err != nil {
		logger.Errorf("auth failed: %v", err)
		return
	}
    ipcClient.StartReceive()

	for i := 0; i < 10; i++ {
		payload := fmt.Sprintf("content-%d", i)
		logger.Info("[process-3] broadcast payload: ", payload)
		err = ipcClient.Broadcast("topic1", []byte(payload), 0)
		if err != nil {
			logger.Errorf("Broadcast failed: %v", err)
			return
		}
		err = ipcClient.BroadcastWithCallBack("topic2", []byte(payload), 0, func(action *client.InterestedAction, occuredErr error) error {
            if action == nil {
                return nil
            }
            logger.Infof("received reply:{ActionName:%s, Txid:%s, Payload:%s, BroadcastClientId:%d}", action.ActionName, action.Txid, action.Payload, action.BroadcastClientId)
            return nil
        })
		if err != nil {
			logger.Errorf("Broadcast failed: %v", err)
			return
		}
		time.Sleep(time.Second)
	}

    time.Sleep(5*time.Second)

}
