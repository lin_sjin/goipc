package main

import (
	"gitee.com/mqyqingkong/goipc/client"
	"gitee.com/mqyqingkong/goipc/log"
	"gitee.com/mqyqingkong/goipc/util"
)

var logger = log.GetLogger("ipc-client1")

func main() {
	network := util.GetEnv("ipc_network", "unix")
	address := util.GetEnv("ipc_address", "../../tiny-goipc.sock")

	logger.Infof("create %s IPC client", network)

	ipcClient := client.New(network, address)

	err := ipcClient.Auth([]byte("process1:didibaba1"))
	if err != nil {
		logger.Errorf("auth failed: %v", err)
		return
	}

	ipcClient.RegisterReceivers("topic1", &process1BroadcastReceiver1{}, &process1BroadcastReceiver2{})
	err = ipcClient.StartReceive()
	if err != nil {
		logger.Error("RegisterReceivers error: ", err)
		return
	}

	ipcClient.Monitor(func() bool {
		return true
	})

	logger.Info("[process-1]started")
	select {}
}

type process1BroadcastReceiver1 struct {
}

func (r *process1BroadcastReceiver1) OnReceive(action *client.InterestedAction) (respPayload []byte) {
	logger.Info("[process1BroadcastReceiver-1]received action:", action)
	return nil
}

type process1BroadcastReceiver2 struct {
}

func (r *process1BroadcastReceiver2) OnReceive(action *client.InterestedAction) (respPayload []byte) {
	logger.Info("[process1BroadcastReceiver-2]received action:", action)
	return nil
}
