package client

import (
	"bytes"
	"errors"
	"strings"
	"sync"
	"time"

	"gitee.com/mqyqingkong/goipc/auth"
	"gitee.com/mqyqingkong/goipc/log"
	"gitee.com/mqyqingkong/goipc/util"
	tinyiotserver "gitee.com/mqyqingkong/tiny-iot-server"
	"gitee.com/mqyqingkong/tiny-iot-server/common"
)

const (
	BroadcastTopic = "broadcast"
	ResponseAction = "ResponseAction"
)

var logger = log.GetLogger("ipc-client")

type Client interface {
	Broadcast(actionName string, payload []byte, expireAfterNowMillis uint32) error
    BroadcastWithCallBack(actionName string, payload []byte, expireAfterNowMillis uint32, callBack Callback) error
	RegisterReceivers(actionName string, receivers ...Receiver)
	StartReceive() error
	Close() error
	Monitor(restartCondition func() bool)
	// Auth 请求授权
	Auth(payload []byte) error
}

func New(network string, serverAddress string) Client {
	var ipcClient Client
	if network == "unix" {
		ipcClient = NewUnix(serverAddress)
	} else {
		ipcClient = NewTcp(serverAddress)
	}
	return ipcClient
}

func NewUnix(serverAddress string) Client {
	c, err := tinyiotserver.NewUnixClient(serverAddress, 0)
	if err != nil {
		panic(err)
	}
	ipc := newIpcClient(c, serverAddress)
	return ipc
}

func NewTcp(serverAddress string) Client {
	c, err := tinyiotserver.NewClient(serverAddress, 0)
	if err != nil {
		panic(err)
	}
	ipc := newIpcClient(c, serverAddress)
	return ipc
}

func newIpcClient(c tinyiotserver.Client, serverAddress string) *ipcClient {
	ipc := &ipcClient{
		serverAddress:        serverAddress,
		receivers:            map[string][]Receiver{},
		lock:                 sync.RWMutex{},
		c:                    c,
		receive:              nil,
		receiveStarted:       false,
		ipcErrored:           make(chan struct{}),
		useUnix:              false,
		authPayload:          []byte{},
		closed:               make(chan struct{}),
		waitResponseReceiver: newWaitResponseReceiver(),
	}
	ipc.clearIpcError()
	ipc.RegisterReceivers(ResponseAction, ipc.waitResponseReceiver)
	return ipc
}

type ipcClient struct {
	serverAddress        string
	receivers            map[string][]Receiver
	lock                 sync.RWMutex
	c                    tinyiotserver.Client
	receive              func() (*common.Msg, error)
	receiveStarted       bool
	ipcErrored           chan struct{}
	useUnix              bool
	authPayload          []byte
	closed               chan struct{}
	waitResponseReceiver *waitResponseReceiver
}

func (ipc *ipcClient) Broadcast(actionName string, payload []byte, expireAfterNowMillis uint32) error {
	txid := util.GenTxid()
	return ipc.broadcast(actionName, txid, payload, expireAfterNowMillis)
}

func (ipc *ipcClient) BroadcastWithCallBack(actionName string, payload []byte, expireAfterNowMillis uint32, callBack Callback) error {
	txid := util.GenTxid()
    var err error
    if callBack != nil {
		ipc.waitResponseReceiver.RegisterCallBack(txid, callBack)
        err = ipc.broadcast(actionName, txid, payload, expireAfterNowMillis)
        if err != nil {
            ipc.waitResponseReceiver.PopCallBack(txid)
        }
	} else {
        err = ipc.broadcast(actionName, txid, payload, expireAfterNowMillis)
    }
	
	return err
}

func (ipc *ipcClient) broadcast(actionName string, txid string, payload []byte, expireAfterNowMillis uint32) error {
	broadcastInternalMsg := common.NewMsg(common.MsgHeader{
		Version:        0,
		ExpireAfterNow: expireAfterNowMillis,
	}, common.MsgBody{
		Topic:   []byte(actionName),
		Payload: util.ToTxPayloadBytesWithTxId(txid, payload),
	})
	broadcastInternalMsg.InitLength()
	err := ipc.c.Submit(BroadcastTopic, broadcastInternalMsg.Bytes(), expireAfterNowMillis)
	if err != nil {
		ipc.ipcErrorOccured()
		return err
	}
	return nil
}

func isReqFail(resp *common.Msg) bool {
	return bytes.HasPrefix(resp.Body.Payload, []byte("FAIL:"))
}

func (ipc *ipcClient) RegisterReceivers(interestedActionName string, receivers ...Receiver) {
	ipc.lock.Lock()
	defer ipc.lock.Unlock()
	ipc.receivers[interestedActionName] = append(ipc.receivers[interestedActionName], receivers...)
}

func (ipc *ipcClient) StartReceive() error {
	ipc.lock.Lock()
	defer ipc.lock.Unlock()
	err := ipc.registerInterestedActions()
	if err != nil {
		return err
	}
	if !ipc.receiveStarted {
		go ipc.startListen()
		ipc.receiveStarted = true
	}
	return nil
}

func (ipc *ipcClient) ipcErrorOccured() {
	select {
	case <-ipc.ipcErrored:
	default:
		close(ipc.ipcErrored)
	}
}

func (ipc *ipcClient) clearIpcError() {
	select {
	case <-ipc.ipcErrored:
		ipc.lock.Lock()
		ipc.ipcErrored = make(chan struct{})
		ipc.lock.Unlock()
	default:
	}
}

func (ipc *ipcClient) Monitor(restartCondition func() bool) {
	go func() {
		for {
			select {
			case <-ipc.closed:
				return
			default:
			}
			<-ipc.ipcErrored
			if restartCondition() {
				logger.Info("begin restarting the ipc client")
				err := ipc.reStart()
				if err == nil {
					ipc.clearIpcError()
					logger.Info("restart the ipc client success")
				}
			} else {
				logger.Info("finish the ipc client")
				return
			}
			time.Sleep(5 * time.Second)
		}

	}()
}

func (ipc *ipcClient) getReceivers(interestedActionName string) []Receiver {
	ipc.lock.RLock()
	defer ipc.lock.RUnlock()
	return ipc.receivers[interestedActionName]
}

func (ipc *ipcClient) reStart() error {
	ipc.lock.Lock()
	defer ipc.lock.Unlock()

	ipc.c.Close()

	var c tinyiotserver.Client
	var err error

	if ipc.useUnix {
		c, err = tinyiotserver.NewUnixClient(ipc.serverAddress, 0)
	} else {
		c, err = tinyiotserver.NewClient(ipc.serverAddress, 0)
	}

	if err != nil {
		return err
	}
	ipc.c = c
	if len(ipc.authPayload) > 0 {
		err = ipc.Auth(ipc.authPayload)
		if err != nil {
			return err
		}
	}
	if len(ipc.receivers) > 0 {
		err = ipc.registerInterestedActions()
		if err != nil {
			return err
		}
	}

	if !ipc.receiveStarted && len(ipc.receivers) > 0 {
		go ipc.startListen()
		ipc.receiveStarted = true
	}
	return err
}

func (ipc *ipcClient) registerInterestedActions() error {
	interestedActionNames := make([]string, 0, len(ipc.receivers))
	for actionName := range ipc.receivers {
		interestedActionNames = append(interestedActionNames, actionName)
	}
	resp, err := ipc.c.Query(BroadcastTopic, []byte(auth.TEST_AUTH)) //同步，验证授权
	if err != nil {
		return err
	}
	if isReqFail(resp) {
		return errors.New(string(resp.Body.Payload))
	}
	receive, err := ipc.c.QueryStreamGroup(BroadcastTopic, []byte(strings.Join(interestedActionNames, ",")))
	if err != nil {
		ipc.receive = nil
	} else {
		ipc.receive = receive
	}
	return err
}

func (ipc *ipcClient) startListen() {
	defer func() {
		ipc.lock.Lock()
		ipc.receiveStarted = false
		ipc.lock.Unlock()
	}()
	for {
		select {
		case <-ipc.closed:
			return
		default:
		}
		if ipc.receive == nil {
			ipc.ipcErrorOccured()
			return
		}
		msg, err := ipc.receive()
		if err != nil {
			ipc.ipcErrorOccured()
			logger.Errorf("error occured: %v \n", err)
			return
		}
		if msg.IsHeartbeat() {
			// log.Printf("receive heartbeat msg:%s", msg)
			continue
		}
		actionName := string(msg.Body.Topic)
		receivers := ipc.getReceivers(actionName)
		if len(receivers) == 0 {
			continue
		}

		txPayload := util.ToTxPayload(msg.Body.Payload)

		for _, receiver := range receivers {
			payload := make([]byte, len(txPayload.Payload))
			copy(payload, txPayload.Payload)
			action := &InterestedAction{
				ActionName:        actionName,
				BroadcastClientId: msg.Header.ClientId,
				Txid:              txPayload.Txid,
				Payload:           payload,
			}
			respPayload := receiver.OnReceive(action)
			if respPayload != nil {
				err := ipc.broadcast(ResponseAction, txPayload.Txid, respPayload, 0)
				if err != nil {
					logger.Errorf("receiver[%T] processes msg error[%v], msg:%s", receiver, err, msg)
				}
			}
		}
	}
}

func (ipc *ipcClient) Close() error {
	err := ipc.c.Close()
	close(ipc.closed)
	return err
}

func (ipc *ipcClient) Auth(payload []byte) error {
	ipc.authPayload = payload
	return ipc.c.Auth(payload)
}
