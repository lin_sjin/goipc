package client

import (
	"fmt"
	"time"

	"github.com/patrickmn/go-cache"
)

type InterestedAction struct {
	ActionName        string
	BroadcastClientId uint32
	Txid              string
	Payload           []byte
}

func (a *InterestedAction) String() string {
	return fmt.Sprintf("[ActionName:%s, BroadcastClientId:%d, Payload:%s]",
		a.ActionName, a.BroadcastClientId, a.Payload)
}

type Receiver interface {
	OnReceive(action *InterestedAction) (respPayload []byte)
}

type OnReceive func(action *InterestedAction) (respPayload []byte)
type Callback func(action *InterestedAction, occuredErr error) error

func (onReceive OnReceive) OnReceive(action *InterestedAction) (respPayload []byte) {
	return onReceive(action)
}

type callbackT struct {
	callBack Callback
	regTime  time.Time //CallBack注册时间，可用于超时处理
}

type waitResponseReceiver struct {
    txCallbackCache *cache.Cache
}

func newWaitResponseReceiver() *waitResponseReceiver {
	r := &waitResponseReceiver{
		txCallbackCache: cache.New(5*time.Minute, 30*time.Minute),
	}
	return r
}

func (r *waitResponseReceiver) RegisterCallBack(txid string, callback Callback) {
    r.txCallbackCache.SetDefault(txid, &callbackT{
		callBack: callback,
		regTime:  time.Now(),
	})
}

func (r *waitResponseReceiver) OnReceive(action *InterestedAction) (respPayload []byte) {
	callbackT := r.PopCallBack(action.Txid)
	if callbackT != nil {
		callbackT.callBack(action, nil)
	}
	return nil
}

func (r *waitResponseReceiver) PopCallBack(txid string) *callbackT {
    txCallback, found := r.txCallbackCache.Get(txid)
    if found {
        r.txCallbackCache.Delete(txid)
        return txCallback.(*callbackT)
    }
    return nil
}
