package auth

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"strings"
	"sync"
	"time"

	"gitee.com/mqyqingkong/goipc/log"
	tinyiotserver "gitee.com/mqyqingkong/tiny-iot-server"
	"gitee.com/mqyqingkong/tiny-iot-server/common"
)

const (
	//请求授权
	REQ_AUTH string = "REQ_AUTH"
	//响应授权
	RES_AUTH string = "RES_AUTH"

	TEST_AUTH string = "TEST_AUTH"
)

var logger = log.GetLogger("auth")

type AuthInfo struct {
	ClientName     string `json:"clientName"`
	Password       string `json:"password"`
	AssignClientId uint32 `json:"assignClientId"`
	ValidSeconds   int    `json:"validSeconds"`
}

type AuthCode struct {
	ClientName  string    //客户端名称
	AuthCode    string    //授权码
	ClientId    uint32    //客户端id
	InvalidTime time.Time //授权失效时间
}

// NewSimpleAuthMsgHandler 授权设备接入。
// authCodeGen：生成授权码。
func NewSimpleAuthMsgHandler(authInfoJsonPath string, authCodeGen func(msg *tinyiotserver.Message) string) tinyiotserver.MessageHandler {
	var authInfos []AuthInfo
	fileBytes, err := ioutil.ReadFile(authInfoJsonPath)
	if err != nil {
		panic(err)
	}
	err = json.Unmarshal(fileBytes, &authInfos)
	if err != nil {
		panic(err)
	}

	h := &simpleAuthMsgHandler{
		MsgHandlerAdapter: tinyiotserver.MsgHandlerAdapter{},
		authCodes:         sync.Map{},
		authInfos:         sync.Map{},
		authCodeGen:       authCodeGen,
	}
	for i := 0; i < len(authInfos); i++ {
		_, exists := h.authInfos.LoadOrStore(authInfos[i].ClientName, &authInfos[i])
		if exists {
			panic(fmt.Errorf("client[%s] exists", authInfos[i].ClientName))
		}
	}
	if authCodeGen == nil {
		h.authCodeGen = func(msg *tinyiotserver.Message) string {
			return time.Now().Format(time.RFC3339Nano)
		}
	}
	return h
}

// simpleAuthMsgHandler 设备接入授权
type simpleAuthMsgHandler struct {
	tinyiotserver.MsgHandlerAdapter
	//clientId : *AuthCode
	authCodes sync.Map
	//clientName : *AuthInfo
	authInfos sync.Map
	//生成授权码
	authCodeGen func(msg *tinyiotserver.Message) string
}

func (h *simpleAuthMsgHandler) OnReceive(requestMsg *tinyiotserver.Message) (newMsg *tinyiotserver.Message) {
	topic := string(requestMsg.Body.Topic)
	if requestMsg.Header.Type == common.POST_WAIT_RESPONSE && topic == REQ_AUTH {
		//请求授权
		reqBody := string(requestMsg.Body.Payload)
		namePass := strings.Split(reqBody, ":")
		if len(namePass) != 2 {
			logger.Infof("auth fail, client[%s] can not be authorized", reqBody)
			return tinyiotserver.FailMsg(topic, "auth invalid")
		}
		//验证用户名密码
		clientName := namePass[0]
		authInfoDbObj, found := h.authInfos.Load(clientName)
		if !found {
			logger.Infof("auth fail, client[%s] can not be authorized", reqBody)
			return tinyiotserver.FailMsg(topic, "auth invalid")
		} else {
			passwordParam := namePass[1]
			if passwordParam != authInfoDbObj.(*AuthInfo).Password {
				logger.Infof("auth fail, client[%s] can not be authorized", reqBody)
				return tinyiotserver.FailMsg(topic, "auth invalid")
			}
		}

		authInfoDb := authInfoDbObj.(*AuthInfo)
		var invalidTime time.Time
		if authInfoDb.ValidSeconds > 0 {
			invalidTime = time.Now().Add(time.Duration(authInfoDb.ValidSeconds) * time.Second)
		}

		//生成授权码
		msgCp := requestMsg.Copy()
		authCode := h.authCodeGen(msgCp)
		h.authCodes.Store(authInfoDb.AssignClientId, &AuthCode{
			ClientName:  authInfoDb.ClientName,
			AuthCode:    authCode,
			ClientId:    authInfoDb.AssignClientId,
			InvalidTime: invalidTime,
		})

		respMsg := &tinyiotserver.Message{
			Msg: common.NewMsg(common.MsgHeader{
				Version:  common.Version,
				Type:     common.RESPONSE,
				ClientId: authInfoDb.AssignClientId,
			},
				common.MsgBody{
					Topic:   []byte(RES_AUTH),
					Payload: []byte(authCode),
				}),
		}

		return respMsg
	} else {
		//验证授权
		clientId := requestMsg.Header.ClientId
		authCodeShouldObj, found := h.authCodes.Load(clientId)
		if !found {
			logger.Infof("auth invalid, clientId[%d] not authorized", clientId)
			return tinyiotserver.FailMsg(topic, "auth invalid")
		}

		authCode := authCodeShouldObj.(*AuthCode)
		if !authCode.InvalidTime.IsZero() && authCode.InvalidTime.Before(time.Now()) {
			logger.Infof("auth invalid, clientId[%d] time out", clientId)
			return tinyiotserver.FailMsg(topic, "auth invalid")
		}

		//验证授权码
		payload := requestMsg.Body.Payload
		if len(payload) < len(authCode.AuthCode) {
			logger.Infof("auth invalid, clientId:%d, payload:%s", clientId, string(payload))
			return tinyiotserver.FailMsg(topic, "auth invalid")
		}

		clientCode := string(payload[:len(authCode.AuthCode)])

		if authCode.AuthCode != clientCode {
			logger.Infof("auth invalid, clientId:%d, clientCode:%s", clientId, clientCode)
			return tinyiotserver.FailMsg(topic, "auth invalid")
		}

		//重新设置消息Payload
		if len(payload) >= len(authCode.AuthCode) {
			requestMsg.SetPayload(payload[len(authCode.AuthCode):])
		}

		//测试验证授权
		if bytes.Equal([]byte(TEST_AUTH), requestMsg.Body.Payload) {
			return tinyiotserver.SuccessMsg(topic)
		}

		//验证通过
		return
	}
}
