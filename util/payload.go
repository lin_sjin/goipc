package util

import (
	"bytes"

	uuid "github.com/satori/go.uuid"
)

var TxPayloadSplit = []byte("<|++|>")

type TxPayload struct {
	Txid    string
	Payload []byte
}

func (tb *TxPayload) Bytes() []byte {
	return ToTxPayloadBytesWithTxId(tb.Txid, tb.Payload)
}

func ToTxPayloadBytes(payload []byte) []byte {
	txid := GenTxid()
	return ToTxPayloadBytesWithTxId(txid, payload)
}

func ToTxPayloadBytesWithTxId(txid string, payload []byte) []byte {
	ret := make([]byte, 0, len(txid)+len(TxPayloadSplit)+len(payload))

	ret = append(ret, txid...)
	ret = append(ret, TxPayloadSplit...)
	ret = append(ret, payload...)
	return ret
}

func ToTxPayload(txPayloadBytes []byte) *TxPayload {
    splitIndex := bytes.Index(txPayloadBytes, TxPayloadSplit)
	if splitIndex < 1 {
		return nil
	}
	tp := &TxPayload{
		Txid:    string(txPayloadBytes[:splitIndex]),
		Payload: txPayloadBytes[splitIndex + len(TxPayloadSplit):],
	}
	return tp
}

func GenTxid() string {
	txid := uuid.NewV4().String()
	return txid
}
