一个轻量的IPC（Inter-Process Communication，进程间通信）组件
==================================================
基于socket实现（tcp、unix）。  

## 一、使用示例
进程3向进程1和进程2发送数据。
### 1、启动ipc-server
```go
    export ipc_network=unix
    export ipc_address=tiny-goipc.sock
    go run main.go
```
### 2、进程3，发送消息
```go
    ipcClient3 := client.New("unix", "tiny-goipc.sock")
    // 接入授权
	ipcClient3.Auth([]byte("process3:didibaba3"))
    // 广播数据，不接受响应
    ipcClient3.Broadcast("topic-test", []byte("from process-3"), 0)
```
### 3、进程1，接收消息
```go
    ipcClient1 := client.New("unix", "tiny-goipc.sock")
    // 接入授权
	ipcClient1.Auth([]byte("process1:didibaba1"))
    // 注册接收逻辑
    ipcClient1.RegisterReceivers("topic-test", client.OnReceive(func(action *client.InterestedAction) (respPayload []byte) {
		logger.Info("received:", action.Payload)
		return nil
	}))
	ipcClient1.StartReceive()
```
### 4、进程2，接收消息
```go
    ipcClient2 := client.New("unix", "tiny-goipc.sock")
    // 接入授权
	ipcClient2.Auth([]byte("process2:didibaba2"))
    // 注册接收逻辑
    ipcClient2.RegisterReceivers("topic-test", client.OnReceive(func(action *client.InterestedAction) (respPayload []byte) {
		logger.Info("received:", action.Payload)
		return []byte("ok")
	}))
	ipcClient2.StartReceive()
```