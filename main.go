package main

import (
	"bytes"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"

	"gitee.com/mqyqingkong/goipc/auth"
	"gitee.com/mqyqingkong/goipc/log"
	"gitee.com/mqyqingkong/goipc/util"
	tinyiotserver "gitee.com/mqyqingkong/tiny-iot-server"
	"gitee.com/mqyqingkong/tiny-iot-server/common"
)

var logger = log.GetLogger("ipc-server")

func main() {
	network := util.GetEnv("ipc_network", "unix")
	address := util.GetEnv("ipc_address", "tiny-goipc.sock")

	logger.Infof("create %s IPC server", network)

	//创建ipcServer
	ipcServer := tinyiotserver.NewServer(address, tinyiotserver.WithNetwork(network), tinyiotserver.WithHeartbeatDuration(10*time.Second))

	//授权验证
	authHandler := auth.NewSimpleAuthMsgHandler("auth_info.json", nil)
	ipcServer.RegisterGlobalPreHandler(authHandler)

	//处理广播消息
	handler := &IpcBroadcastMsgHandler{}
	ipcServer.RegisterHandler("broadcast", handler)

	//启动服务
	go func() {
		err := ipcServer.Start()
		if err != nil {
			logger.Info("start server error:", err)
			return
		}
	}()

	//释放资源
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit
	ipcServer.Close()
}

type IpcBroadcastMsgHandler struct {
}

func (h *IpcBroadcastMsgHandler) OnReceive(requestMsg *tinyiotserver.Message) (newMsg *tinyiotserver.Message) {
	logger.Infof("[IpcBroadcastMsgHandler] received msg from client:%d\n", requestMsg.Header.ClientId)
	return requestMsg
}

func (h *IpcBroadcastMsgHandler) OnResponse(responseMsg *tinyiotserver.Message, requestMsg *tinyiotserver.Message) (replyToClientMsg *tinyiotserver.Message) {
	if requestMsg.Header.Type.IsPost() || responseMsg.IsHeartbeat() || responseMsg.IsEmpty(){
		return responseMsg
	}

	//客户端广播时，发送的消息topic是固定的（“broadcast”），消息体里存放的才是真正的广播消息
	internalBroadcastMsg, err := common.ReadMsg(bytes.NewReader(responseMsg.Body.Payload))
	if err != nil {
		return tinyiotserver.FailMsg(string(requestMsg.Body.Topic), err.Error())
	}

	internalBroadcastTopic := string(internalBroadcastMsg.Body.Topic)
	//获取请求里，感兴趣的topic
	interestedActions := strings.Split(string(requestMsg.Body.Payload), ",")
	//是否对该消息感兴趣
	interested := false
	for i := 0; i < len(interestedActions); i++ {
		if interestedActions[i] == internalBroadcastTopic {
			interested = true
		}
	}
	if !interested {
		return nil
	}

    internalBroadcastMsg.Header.ClientId = responseMsg.Header.ClientId
	replyToClientMsg = &tinyiotserver.Message{
		Msg:          internalBroadcastMsg,
		ReceivedTime: responseMsg.ReceivedTime,
		ExpiredTime:  responseMsg.ExpiredTime,
		No:           responseMsg.No,
	}
	return replyToClientMsg
}
